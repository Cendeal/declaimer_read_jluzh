import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from '@/views/home/Home.vue'
import VoiceDetail from '@/views/home/pages/VoiceDetail.vue'
import Me from '@/views/me/Me.vue'
import Rank from '@/views/rank/Rank.vue'
import Declaimer from '@/views/Declaimer.vue'
import AddWork from "./views/me/pages/AddWork";
import About from "./views/about/About";
Vue.use(VueRouter)

const router = new VueRouter({
    mode: "history",
    routes: [
        {
            path: '/declaimer',
            component:Declaimer,
            children: [
                {
                    path: '/declaimer/home',
                    component: Home,
                },
                {
                    path: '/declaimer/me',
                    component: Me
                },
                {
                    path: '/declaimer/rank',
                    component: Rank
                }
            ]
        },
        {
            path: '/declaimer/voiceDetail/:id',
            component:VoiceDetail
        },
        {
            path: '/declaimer/addWork',
            component:AddWork
        },
        {
            path: '/declaimer/about',
            component:About
        }


    ]
})
export default router