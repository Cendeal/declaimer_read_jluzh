const localcache={
    state:{
        me:{
            works:{
                id:" 作品id",
                title: "作品名称",
                record: "录音链接",
                content: "录音内容",
                duration: "录音时长",
                status: "审核状态true 通过 false 未通过",
                thumbs: "点赞数",
                upTime: "上传时间",
                visitor: "浏览人数",
                nickname: "作者昵称",
                ranks: "排名"
            },
            msg_board:{
                global:'',
                your:''
            }
        },
        home:{
        },
        rank:{
        },
        add:{
        },
        player:{
        }
    },
    mutations:{
        updateLocalCacheMe(state,works){
            state.me.works=works
        },
        addMsgToYou(state,msg){
            state.me.msg_board.your=msg
        }
    },
    actions:{
        saveLocalCacheMe({commit,state},me){
            commit('updateLocalCacheMe',me)
            let loc = localStorage.getItem('declaimer_me')
            if (loc !=null){
                let count = parseInt(state.me.works.thumb)-parseInt(JSON.parse(loc).thumb)
                if(count>0){
                        commit('addMsgToYou','你获得新的'+count+'个赞啦!')
                        commit('updateMsgCounter',count)
                }else {
                    commit('addMsgToYou','')
                }
            }
            localStorage.setItem('declaimer_me',JSON.stringify(state.me.works))
        },
        initLocalCacheMeFromLoc({commit}){
            let loc = localStorage.getItem('declaimer_me')
            if(loc!=null){
                let loc_me = JSON.parse(loc)
                commit('updateLocalCacheMe',loc_me)
            }
        }
    },
    getters:{
    }
}