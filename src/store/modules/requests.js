import axios from 'axios'

const host = 'https://read.jluzh.com'
const requests = {
    state: {
        api: {
            add_works: {
                url: host + '/api/add/works',
                method: 'post',
                data: null
            },
            add_visitor: {
                url: host + '/api/add/visitor',
                method: 'get',
                params: {
                    wId: ''
                }
            },
            check_nickName: {
                url: host + '/api/declaimer/nickname',
                method: 'get',
                params: {
                    nickname: ''     //'朗读者昵称'
                }
            },
            check_worksName: {
                url: host + '/api/works/name',
                method: 'get',
                params: {
                    name: ''        //'作品名称'
                }
            },
            self_works: {
                url: host + '/api/works/info',
                method: 'get',
                params: {}
            },
            works_list: {
                url: host + '/api/works/list',
                method: 'get',
                params: {
                    pageNum: 1,
                    pageSize: 10,
                    getWay: true
                }
            },
            thumbs: {
                url: host + '/api/thumbs/works',
                params: {
                    wId: '作品id'
                }
            },
        }
    },
    mutations: {
        getMusic: (state, obj) => {
            state.api.self_works.params = obj
        },
        addWorkInformation: (state, obj) => {
            state.api.add_works.data = obj
        },
        addNickName(state, name) {
            state.api.check_nickName.params = name
        },
        addWorkName(state, name) {
            state.api.check_worksName.params = name
        },
        addWorkListInfo(state, page) {
            state.api.works_list.params = page

        },
        addVisitor(state, params) {
            state.api.add_visitor.params = params
        },
        addThumbs(state, wId) {
            state.api.thumbs.params = wId
        }
    },
    getters: {},
    actions: {
        addWorks({commit, state}, obj) {
            let form = new FormData()
            for (let i in obj) {
                form.append(i, obj[i])
            }
            commit('addWorkInformation', form)
            return axios(state.api.add_works)
        },
        actionAddVisitor({commit, state}, param) {
            commit('addVisitor', param)
            return axios(state.api.add_visitor)
        },
        checkNickName({commit, state}, name) {
            commit('addNickName', name)
            return axios(state.api.check_nickName)
        },
        checkWorkName({commit, state}, name) {
            commit('addWorkName', name)
            return axios(state.api.check_worksName)
        },
        getWorksList({commit, state}, page) {
            commit("addWorkListInfo", page)
            return axios(state.api.works_list)
        },
        giveThumbs({commit, state}, wId) {
            commit("addThumbs", wId)
            return axios(state.api.thumbs)
        },
        getSelfWorks({commit, state}, wId) {
            if (wId != null) {
                commit('getMusic', wId)
            }
            return axios(state.api.self_works)
        }
    }
}
export default requests