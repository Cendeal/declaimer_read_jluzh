const uiSate = {
    state: {
        my_msg_counter: '0',
        music: {
            id: null,
            work_content: null,
            duration: null,
            status: null,
            thumb: null,
            upload: null,
            view: null,
            ranks: null,
            title: '未知',
            artist: '未知',
            src: null,
            current_time: 0,
            pic: 'https://moeplayer.b0.upaiyun.com/aplayer/secretbase.jpg',
            theme: '#41b883'
        },
        backend_player: {
            control: {
                play: false,
                show: false,
            },
            music: {
                id: null,
                work_content: null,
                duration: null,
                status: null,
                thumb: null,
                upload: null,
                view: null,
                ranks: null,
                title: '未知',
                artist: '未知',
                src: null,
                current_time: 0,
                pic: 'https://moeplayer.b0.upaiyun.com/aplayer/secretbase.jpg',
                theme: '#41b883'
            }
        },
        top_bar: '#212121',
        top_tile: '享念',
        back_to: "/declaimer/home"
    },
    mutations: {
        updateMsgCounter(state, val) {
            if (this.val > 99) {
                state.my_msg_counter = "99+"
            } else {
                state.my_msg_counter = val
            }
            state.my_msg_counter = val
        },
        selectVoice(state, val) {
            state.music = val
        },
        updateBackend(state, val) {
            state.backend_player.control.show = val
        },
        updateBackendPlay(state, val) {
            state.backend_player.control.play = val
        },
        updateBackendMusic(state, val) {
            state.backend_player.music = val
        },
        updateTopBar(state, val) {
            state.top_bar = val
        },
        updateTopTile(state, val) {
            state.top_tile = val
        },
        updateBackTo(state, val) {
            state.back_to = val
        },
        updateCurrentTime(state, val) {
            state.backend_player.music.current_time = val
        },
        updateMusicCurrentTime(state, val) {
            state.music.current_time = val
        }
    }
}
export default uiSate