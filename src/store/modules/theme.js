/**
 * this is a theme setting js
 * @type {{mutations: {}, state: {secondary: string, divider: string, success: string, background: {chip: string, default: string, paper: string}, name: string, warning: string, text: {secondary: string, hint: string, alternate: string, disabled: string, primary: string}, error: string, track: string, primary: string, info: string}, getters: {}}}
 */

const theme = {
    state: {
        name: 'own',
        primary: '#212121',
        secondary: '#ff4081',
        success: '#4caf50',
        warning: '#fdd835',
        info: '#2196f3',
        error: '#f44336',
        track: '#bdbdbd',
        text: {
            primary: 'rgba(0, 0, 0, 0.87)',
            secondary: 'gba(0, 0, 0, 0.54)',
            alternate: '#fff',
            disabled: 'rgba(0, 0, 0, 0.38)',
            hint: 'rgba(0, 0, 0, 0.38)' // 提示文字颜色
        },
        divider: 'rgba(0, 0, 0, 0.12)',
        background: {
            paper: '#fff',
            chip: '#e0e0e0',
            default: '#fafafa'
        }
    },
    mutations: {
        upateTheme(state, theme) {
            for (let key in state.theme) {
                if (theme.hasOwnProperty(key)) {
                    state[key] = theme[key]
                }
            }
        }
    },
    getters: {}
}
export default theme