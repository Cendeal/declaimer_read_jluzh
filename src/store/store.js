import Vue from 'vue'
import Vuex from 'vuex'
import requests from './modules/requests'
import theme from './modules/theme'
import uiState from './modules/uiSates'
Vue.use(Vuex)
const store = new Vuex.Store({
        modules: {
            requests: requests,
            theme: theme,
            uiState:uiState,
        }
    }
)
export default store