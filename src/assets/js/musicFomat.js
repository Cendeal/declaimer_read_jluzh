function needFormat() {
    return {
        id: 'id',
        title: 'title',
        src: 'record',
        work_content: "content",
        time_info: "duration",
        status: "status",
        thumb: "thumbs",
        upload: "upTime",
        view: "visitor",
        artist: "nickname",
        ranks: "ranks",
        theme: '',
        current: 0,
        pic: ''
    }
}

function randomColor() {
    let r = Math.floor(Math.random() * 220)
    let g = Math.floor(Math.random() * 220)
    let b = Math.floor(Math.random() * 220)
    let rgba = 'rgba(' + r.toString() + ',' + g.toString() + ',' + b.toString() + ',1)'
    return rgba
}

function musicFormat(origin) {
    let dict = needFormat()
    for (let k in dict) {
        if (k === 'theme') {
            dict['theme'] = randomColor()
            continue
        } else if (k === 'pic') {
            dict['pic'] = 'https://moeplayer.b0.upaiyun.com/aplayer/secretbase.jpg'
            continue
        }
        let t_key = dict[k]
        dict[k] = origin[t_key]
    }
    return dict
}

export {musicFormat}