const demoList = [{
    id: '0001',
    title: 'demo-清平调',
    src: 'https://read.jluzh.com/static/records/demo.mp3',
    work_content: "这里的数据为你第一次加载的进来看到的界面demo,刷新就可以获取新的数据。\n",
    time_info: "04:29",
    status: "status",
    thumb: "520",
    upload: "2019-04-15",
    view: "1000",
    artist: "demo-music",
    ranks: "1",
    theme: 'green',
    current: 0,
    pic: 'https://moeplayer.b0.upaiyun.com/aplayer/secretbase.jpg'
},
    {
        id: '0001',
        title: 'demo-清平调',
        src: 'https://read.jluzh.com/static/records/demo.mp3',
        work_content: "清平调三首\n" +
            "李白\n" +
            "其一\n" +
            "云想衣裳花想容，春风拂槛露华浓。若非群玉山头见，会向瑶台月下逢。\n" +
            "其二\n" +
            "一枝红艳露凝香，云雨巫山枉断肠。借问汉宫谁得似，可怜飞燕倚新妆。\n" +
            "其三\n" +
            "名花倾国两相欢，长得君王带笑看。解释春风无限恨，沉香亭北倚阑干。\n" +
            "\n" +
            "    有关李白的这三首《清平调》，是李白在长安期间创作的流传最广、知名度最高的诗歌之一。\n" +
            "据说在唐玄宗开元年间，宫中曾经在兴庆池东面的沉香亭畔，栽种了不少名贵的牡丹，到了花开时节，紫红，浅红，全白，各色相间，煞是好看。\n" +
            "一日，唐玄宗骑着心爱的照夜白马，杨太真，即杨贵妃则乘了一乘小轿，一同前来赏花，同时带着当时宫中最著名的乐师，即大名鼎鼎的李龟年。\n" +
            "李龟年看到皇帝与杨玉环兴趣盎然地在赏花，便令他那班梨园弟子拿出乐器，准备奏乐起舞为皇上与贵妃助兴，唐玄宗却说到：“赏名花，对爱妃，哪能还老听这些陈词旧曲呢？”\n" +
            "于是急召翰林学士李白进宫，李白进得宫来，略一思索，便有了主意，很快下笔如飞，一挥而就，在金花笺上写了三首《清平调》诗送上，唐玄宗看了十分满意，当即便令梨园弟子奏起丝竹，李龟年展喉而歌，杨贵妃拿着玻璃七宝杯，倒上西凉州进贡的葡萄美酒，边饮酒边赏歌，不觉喜上眉梢，唐玄宗一见愈发兴起，忍不住也亲自吹起玉笛来助兴，每到一首曲终之际，都要延长乐曲，重复演奏，尽兴方止。\n" +
            "杨贵妃饮罢美酒，听完妙曲，遂款款下拜，向唐玄宗深表谢意。这段出自《杨太真外传》的传奇故事，也许有不少夸张、虚构的成份。但是不容置疑的是，虽然历史上描写皇帝身边女子的诗歌数不胜数，但是要论起既能够得到当事人的喜爱，又受到后人一致好评的却不多，而其中最著名的恐怕非李白这三首《清平调》莫属了。\n",
        time_info: "04:29",
        status: "status",
        thumb: "520",
        upload: "2019-04-15",
        view: "1000",
        artist: "demo-music",
        ranks: "1",
        theme: 'green',
        current: 0,
        pic: 'https://moeplayer.b0.upaiyun.com/aplayer/secretbase.jpg'
    },
    {
        id: '0001',
        title: 'demo-清平调',
        src: 'https://read.jluzh.com/static/records/demo.mp3',
        work_content: "清平调三首\n" +
            "李白\n" +
            "其一\n" +
            "云想衣裳花想容，春风拂槛露华浓。若非群玉山头见，会向瑶台月下逢。\n" +
            "其二\n" +
            "一枝红艳露凝香，云雨巫山枉断肠。借问汉宫谁得似，可怜飞燕倚新妆。\n" +
            "其三\n" +
            "名花倾国两相欢，长得君王带笑看。解释春风无限恨，沉香亭北倚阑干。\n" +
            "\n" +
            "    有关李白的这三首《清平调》，是李白在长安期间创作的流传最广、知名度最高的诗歌之一。\n" +
            "据说在唐玄宗开元年间，宫中曾经在兴庆池东面的沉香亭畔，栽种了不少名贵的牡丹，到了花开时节，紫红，浅红，全白，各色相间，煞是好看。\n" +
            "一日，唐玄宗骑着心爱的照夜白马，杨太真，即杨贵妃则乘了一乘小轿，一同前来赏花，同时带着当时宫中最著名的乐师，即大名鼎鼎的李龟年。\n" +
            "李龟年看到皇帝与杨玉环兴趣盎然地在赏花，便令他那班梨园弟子拿出乐器，准备奏乐起舞为皇上与贵妃助兴，唐玄宗却说到：“赏名花，对爱妃，哪能还老听这些陈词旧曲呢？”\n" +
            "于是急召翰林学士李白进宫，李白进得宫来，略一思索，便有了主意，很快下笔如飞，一挥而就，在金花笺上写了三首《清平调》诗送上，唐玄宗看了十分满意，当即便令梨园弟子奏起丝竹，李龟年展喉而歌，杨贵妃拿着玻璃七宝杯，倒上西凉州进贡的葡萄美酒，边饮酒边赏歌，不觉喜上眉梢，唐玄宗一见愈发兴起，忍不住也亲自吹起玉笛来助兴，每到一首曲终之际，都要延长乐曲，重复演奏，尽兴方止。\n" +
            "杨贵妃饮罢美酒，听完妙曲，遂款款下拜，向唐玄宗深表谢意。这段出自《杨太真外传》的传奇故事，也许有不少夸张、虚构的成份。但是不容置疑的是，虽然历史上描写皇帝身边女子的诗歌数不胜数，但是要论起既能够得到当事人的喜爱，又受到后人一致好评的却不多，而其中最著名的恐怕非李白这三首《清平调》莫属了。\n",
        time_info: "04:29",
        status: "status",
        thumb: "520",
        upload: "2019-04-15",
        view: "1000",
        artist: "demo-music",
        ranks: "1",
        theme: 'green',
        current: 0,
        pic: 'https://moeplayer.b0.upaiyun.com/aplayer/secretbase.jpg'
    }
    ,
    {
        id: '0001',
        title: 'demo-清平调',
        src: 'https://read.jluzh.com/static/records/demo.mp3',
        work_content: "清平调三首\n" +
            "李白\n" +
            "其一\n" +
            "云想衣裳花想容，春风拂槛露华浓。若非群玉山头见，会向瑶台月下逢。\n" +
            "其二\n" +
            "一枝红艳露凝香，云雨巫山枉断肠。借问汉宫谁得似，可怜飞燕倚新妆。\n" +
            "其三\n" +
            "名花倾国两相欢，长得君王带笑看。解释春风无限恨，沉香亭北倚阑干。\n" +
            "\n" +
            "    有关李白的这三首《清平调》，是李白在长安期间创作的流传最广、知名度最高的诗歌之一。\n" +
            "据说在唐玄宗开元年间，宫中曾经在兴庆池东面的沉香亭畔，栽种了不少名贵的牡丹，到了花开时节，紫红，浅红，全白，各色相间，煞是好看。\n" +
            "一日，唐玄宗骑着心爱的照夜白马，杨太真，即杨贵妃则乘了一乘小轿，一同前来赏花，同时带着当时宫中最著名的乐师，即大名鼎鼎的李龟年。\n" +
            "李龟年看到皇帝与杨玉环兴趣盎然地在赏花，便令他那班梨园弟子拿出乐器，准备奏乐起舞为皇上与贵妃助兴，唐玄宗却说到：“赏名花，对爱妃，哪能还老听这些陈词旧曲呢？”\n" +
            "于是急召翰林学士李白进宫，李白进得宫来，略一思索，便有了主意，很快下笔如飞，一挥而就，在金花笺上写了三首《清平调》诗送上，唐玄宗看了十分满意，当即便令梨园弟子奏起丝竹，李龟年展喉而歌，杨贵妃拿着玻璃七宝杯，倒上西凉州进贡的葡萄美酒，边饮酒边赏歌，不觉喜上眉梢，唐玄宗一见愈发兴起，忍不住也亲自吹起玉笛来助兴，每到一首曲终之际，都要延长乐曲，重复演奏，尽兴方止。\n" +
            "杨贵妃饮罢美酒，听完妙曲，遂款款下拜，向唐玄宗深表谢意。这段出自《杨太真外传》的传奇故事，也许有不少夸张、虚构的成份。但是不容置疑的是，虽然历史上描写皇帝身边女子的诗歌数不胜数，但是要论起既能够得到当事人的喜爱，又受到后人一致好评的却不多，而其中最著名的恐怕非李白这三首《清平调》莫属了。\n",
        time_info: "04:29",
        status: "status",
        thumb: "520",
        upload: "2019-04-15",
        view: "1000",
        artist: "demo-music",
        ranks: "1",
        theme: 'green',
        current: 0,
        pic: 'https://moeplayer.b0.upaiyun.com/aplayer/secretbase.jpg'
    },
    {
        id: '0001',
        title: 'demo-清平调',
        src: 'https://read.jluzh.com/static/records/demo.mp3',
        work_content: "清平调三首\n" +
            "李白\n" +
            "其一\n" +
            "云想衣裳花想容，春风拂槛露华浓。若非群玉山头见，会向瑶台月下逢。\n" +
            "其二\n" +
            "一枝红艳露凝香，云雨巫山枉断肠。借问汉宫谁得似，可怜飞燕倚新妆。\n" +
            "其三\n" +
            "名花倾国两相欢，长得君王带笑看。解释春风无限恨，沉香亭北倚阑干。\n" +
            "\n" +
            "    有关李白的这三首《清平调》，是李白在长安期间创作的流传最广、知名度最高的诗歌之一。\n" +
            "据说在唐玄宗开元年间，宫中曾经在兴庆池东面的沉香亭畔，栽种了不少名贵的牡丹，到了花开时节，紫红，浅红，全白，各色相间，煞是好看。\n" +
            "一日，唐玄宗骑着心爱的照夜白马，杨太真，即杨贵妃则乘了一乘小轿，一同前来赏花，同时带着当时宫中最著名的乐师，即大名鼎鼎的李龟年。\n" +
            "李龟年看到皇帝与杨玉环兴趣盎然地在赏花，便令他那班梨园弟子拿出乐器，准备奏乐起舞为皇上与贵妃助兴，唐玄宗却说到：“赏名花，对爱妃，哪能还老听这些陈词旧曲呢？”\n" +
            "于是急召翰林学士李白进宫，李白进得宫来，略一思索，便有了主意，很快下笔如飞，一挥而就，在金花笺上写了三首《清平调》诗送上，唐玄宗看了十分满意，当即便令梨园弟子奏起丝竹，李龟年展喉而歌，杨贵妃拿着玻璃七宝杯，倒上西凉州进贡的葡萄美酒，边饮酒边赏歌，不觉喜上眉梢，唐玄宗一见愈发兴起，忍不住也亲自吹起玉笛来助兴，每到一首曲终之际，都要延长乐曲，重复演奏，尽兴方止。\n" +
            "杨贵妃饮罢美酒，听完妙曲，遂款款下拜，向唐玄宗深表谢意。这段出自《杨太真外传》的传奇故事，也许有不少夸张、虚构的成份。但是不容置疑的是，虽然历史上描写皇帝身边女子的诗歌数不胜数，但是要论起既能够得到当事人的喜爱，又受到后人一致好评的却不多，而其中最著名的恐怕非李白这三首《清平调》莫属了。\n",
        time_info: "04:29",
        status: "status",
        thumb: "520",
        upload: "2019-04-15",
        view: "1000",
        artist: "demo-music",
        ranks: "1",
        theme: 'green',
        current: 0,
        pic: 'https://moeplayer.b0.upaiyun.com/aplayer/secretbase.jpg'
    }
]


export {demoList}