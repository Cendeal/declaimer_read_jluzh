import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/store'
import theme from 'muse-ui/lib/theme';
import Toast from 'muse-ui-toast';
import {musicFormat} from "./assets/js/musicFomat";
import MuseUI from 'muse-ui'
Vue.use(Toast);
Vue.use(MuseUI)
Vue.prototype.$theme = theme
Vue.prototype.$musicFormat = musicFormat

Vue.config.productionTip = true

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
